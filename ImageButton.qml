import QtQuick 2.3

Rectangle {
    property string image;
    property string imageHover: image;
    property string imageClick: imageHover;

    property alias color : button.color;
    property color colorHover: color;
    property color colorClick: colorHover;

    id: button;

    signal clicked;
    signal entered;
    signal exited;

    MouseArea {
        anchors.fill: parent;
        hoverEnabled: true;
        cursorShape: Qt.PointingHandCursor;
        propagateComposedEvents: true;

        onEntered: {
            button.entered();
            button.state = "hovered";
        }
        onExited: {
            button.exited();
            button.state = "";
        }

        onPressed: button.state = "click";
        onReleased: button.state = "hovered";

        onClicked: {
            button.clicked();
        }
    }

    Image {
        id: buttonImage
        source: image;
        anchors.fill: parent;
    }


    states: [
        State {
            name: "hovered";
            PropertyChanges {target: button; color: colorHover;}
            PropertyChanges {target: buttonImage; source: imageHover;}
        },
        State {
            name: "click";
            PropertyChanges {target: button; color: colorClick;}
            PropertyChanges {target: buttonImage; source: imageClick;}
        }

    ]
}
