#ifndef LENSMANAGER_H
#define LENSMANAGER_H

#include "lens.h"

#include <QObject>
#include <QList>
#include <QQmlListProperty>
#include <QImage>

class LensManager : public QObject
{
    Q_OBJECT
public:
    explicit LensManager(QObject *parent = 0);

    Q_PROPERTY(QQmlListProperty<Lens> lens READ lens NOTIFY lensChanged)
    Q_PROPERTY(bool wireframe READ wireframe WRITE setWireframe NOTIFY wireframeChanged)
    Q_PROPERTY(int imageWidth READ imageWidth NOTIFY imageRectChanged)
    Q_PROPERTY(int imageHeight READ imageHeight NOTIFY imageRectChanged)
    Q_PROPERTY(Lens* selectedLens READ selectedLens NOTIFY selectedLensChanged)
    Q_PROPERTY(int cameraZ READ cameraZ WRITE setCameraZ NOTIFY cameraZChanged)
    Q_PROPERTY(float fov READ fov WRITE setFov NOTIFY fovChanged())
    Q_PROPERTY(unsigned int maxDepth READ maxDepth WRITE setMaxDepth NOTIFY maxDepthChanged())


    QQmlListProperty<Lens> lens();
    QList<Lens*> getLensList() { return m_lens; }
    bool wireframe() const { return m_wireframe; }
    void setWireframe(bool wireframe);
    bool lock() const { return m_lock; }
    void setLock(bool lock);
    int imageWidth() const { return m_imageRect.width(); }
    int imageHeight() const { return m_imageRect.height(); }
    Lens* selectedLens() const { return m_selectedLens; }
    float cameraZ() const { return m_cameraZ; }
    float fov() const { return m_fov; }
    unsigned int maxDepth() const { return m_maxDepth; }


private:
    QList<Lens*> m_lens;
    bool m_wireframe = false;
    bool m_lock = true;
    QRect m_imageRect;
    Lens* m_selectedLens = nullptr;
    float m_cameraZ = 1.0f;
    float m_fov = 90.0f;
    unsigned int m_maxDepth = 6;

signals:
    void lensDeleted();
    void lensAdded();
    void lensChanged();
    void wireframeChanged();
    void lockChanged();
    void imageRectChanged();
    void selectedLensChanged();
    void cameraZChanged();
    void fovChanged();
    void maxDepthChanged();

public slots:
    void addLens();
    void deleteLens(int position);
    void deleteLens(Lens* lens);
    void moveLensUp(Lens* lens);
    void moveLensDown(Lens* lens);
    void toggleWireframe();
    void onLensChanged();
    void onImageLoaded(QImage image);
    void onImageClosed();
    void changeSelected(Lens* lens);
    void setCameraZ(float cameraZ);
    void setFov(float fov);
    void setMaxDepth(unsigned int maxDepth);

};

#endif // LENSMANAGER_H
