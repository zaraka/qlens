#ifndef SETTINGSINTERFACE_H
#define SETTINGSINTERFACE_H

#include <QObject>
#include <QSettings>

class SettingsInterface : public QObject
{
    Q_OBJECT
public:
    explicit SettingsInterface(QObject *parent = 0);
    Q_INVOKABLE QVariant get(QVariant key, QVariant defVal = "");
    Q_INVOKABLE void set(QVariant key, QVariant value);
signals:
    void change();
public slots:

private:
    QSettings m_settings;
};

#endif // SETTINGSINTERFACE_H
