#include "settingsinterface.h"
#include "opencvpaintitem.h"
#include "lensmanager.h"

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QQuickView>
#include <QDebug>

using namespace std;

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);
    app.setApplicationName("QLens");
    app.setOrganizationName("fit students");
    app.setOrganizationDomain("fit.vutbr.cz");

    //register QLens specific types
    qmlRegisterType<SettingsInterface>("QLens.core", 0, 1, "SettingsInterface");
    qmlRegisterType<LensManager>("QLens.core", 0, 1, "LensManager");
    qmlRegisterType<OpenCVPaintItem>("QLens.core", 0, 1, "OpencvViewport");
    qmlRegisterType<Lens>("QLens.core", 0, 1, "Lens");

    QQmlApplicationEngine engine;

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    OpenCVPaintItem *opencv = engine.rootObjects().first()->findChild<OpenCVPaintItem*>("opencvViewport");
    LensManager *manager = engine.rootObjects().first()->findChild<LensManager*>("lensManager");

    if(opencv == 0 || manager == 0) {
        qWarning() << "Can't load QML objects";
    } else {
        opencv->setLensManager(manager);
    }

    return app.exec();
}
