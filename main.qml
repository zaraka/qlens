import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Dialogs 1.2
import QLens.core 0.1

ApplicationWindow {
    id: window;
    visible: true
    width: settings.get("window/width", 800);
    height: settings.get("window/height", 600);
    x: settings.get("window/xpos", 0);
    y: settings.get("window/ypos", 0);
    title: qsTr("QLens");

    onClosing: {
        settings.set("window/width", width);
        settings.set("window/height", height);
        settings.set("window/xpos", x);
        settings.set("window/ypos", y);
    }

    SettingsInterface {
        id: settings;
    }

    LensManager {
        id: lens_manager;
        objectName: "lensManager";
    }

    Style {
        id: style;
    }

    Action {
            id: openAction;
            text: "&Open image";
            shortcut: "Ctrl+O";
            tooltip: qsTr("Open an image");
            onTriggered: {
                imageDialog.open();
            }
    }

    Action {
        id: settingsAction;
        text: "Settings";
        tooltip: qsTr("Open settings");
        onTriggered: {
            var settingsComponent = Qt.createComponent("SettingsWindow.qml")
            if (settingsComponent.status === Component.Ready) {
                var wnd = settingsComponent.createObject(window, {manager: lens_manager});
                wnd.x = window.x + (window.width - wnd.width) / 2;
                wnd.y = window.y + (window.height - wnd.height) / 2;
                wnd.show();
            } else if (settingsComponent.status === Component.Error){
                console.error("ERROR while creating settings window");
            }
        }
    }

    Action {
        id: saveAction;
        text: "&Save image";
        shortcut: "Ctrl+S";
        tooltip: qsTr("Save an image");
        onTriggered: {
            saveDialog.open();
        }
    }

    Action {
        id: closeImageAction;
        text: "&Close image";
        shortcut: "Ctrl+C";
        tooltip: qsTr("Close an image");
        onTriggered: {
            opencv_viewport.closeFile();
        }
    }

    Action {
        id: closeAction;
        text: "&Close application";
        shortcut: "Ctrl+X";
        tooltip: qsTr("Close application");
        onTriggered: {
            Qt.quit();
        }
    }

    Action {
        id: newAction;
        shortcut: "l";
        onTriggered: {
            lens_manager.addLens();
        }
    }
    Action {
        id: upAction;
        shortcut: "up";
        onTriggered: {
            lens_manager.moveLensUp(lens_manager.selectedLens);
        }
    }
    Action {
        id: downAction;
        shortcut: "down";
        onTriggered: {
            lens_manager.moveLensDown(lens_manager.selectedLens);
        }
    }
    Action {
        id: deleteAction;
        shortcut: "delete";
        onTriggered: {
            lens_manager.deleteLens(lens_manager.selectedLens);
        }
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("File");
            MenuItem { action: openAction }
            MenuItem { action: closeImageAction }
            MenuItem { action: settingsAction }
            MenuItem { action: closeAction }
        }
        Menu {
            title: qsTr("Help")
            MenuItem {
                text: qsTr("&About")
                onTriggered: about.open()
            }
        }

        style: MenuBarStyle {
            background: Rectangle {
                color: style.foregroundColor
                border.color: style.backgroundColor
            }

        }
    }

    Rectangle {
        id: view;
        anchors.fill: parent;
        color: style.backgroundColor;

        Rectangle {
            id: leftPanel
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width: 230
            color: style.foregroundColor
            anchors.margins: 4

            Rectangle {
                id: toolbar
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: 28
                color: style.accentColor
                z: 5

                ImageButton {
                    id: add_lens_button
                    anchors.top: parent.top
                    anchors.left: parent.left
                    height: 24
                    width: 24
                    image: "qrc:/assets/add.png"
                    imageHover: "qrc:/assets/add_hover.png";
                    imageClick: "qrc:/assets/add_clicked.png";
                    color : "transparent";
                    colorHover: "transparent";
                    colorClick: "transparent";
                    anchors.margins: 2
                    onClicked: {
                        lens_manager.addLens();
                    }
                }
            }


            Rectangle {
                id: lensContainer
                color: "transparent"

                anchors {
                    left: parent.left
                    right: parent.right
                    top: toolbar.bottom
                    bottom: parent.bottom
                }

                ListView {
                    id: lensList

                    anchors.fill: parent
                    anchors.topMargin: 3
                    model: lens_manager.lens
                    spacing: 2
                    delegate: LensView {
                        manager: lens_manager
                        container: lensContainer
                        lens: model.modelData
                    }

                    ScrollBar {
                        flickable: lensList;
                        otherSide: false;
                        hideScrollBarsWhenStopped: true;
                    }
                }
            }
        }

        Rectangle {
            id: opencv
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.left: leftPanel.right
            anchors.margins: 4
            color: style.foregroundColor

            OpencvViewport {
                id: opencv_viewport
                objectName: "opencvViewport";
                anchors.fill: parent

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.warn("clicked")
                    }

                    onWheel: {
                        if (wheel.modifiers & Qt.ControlModifier) {
                            if(wheel.angleDelta.y > 0){
                                opencv_viewport.zoom(true);
                            } else {
                                opencv_viewport.zoom(false);
                            }
                        }
                    }
                }
            }
        }
    }

    MessageDialog {
        id: about
        title: "About"
        text: "Nikita Vaňků\nJán Brída."
    }


    FileDialog {
        id: imageDialog;
        nameFilters: [ "Image files (*.jpg *.png)", "All files (*)" ];
        folder: shortcuts.home;
        title: "Please chose an image";
        onAccepted: {
            opencv_viewport.loadNewFile(imageDialog.fileUrl);
        }
        onRejected: {
            console.log("Canceled");
        }
    }

    FileDialog {
        id: saveDialog;
        nameFilters: [ "Image files (*.jpg *.png)", "All files (*)" ];
        folder: shortcuts.home;
        title: "Please chose an image";
        selectExisting: true;
        onAccepted: {
            opencv_viewport.saveFile(imageDialog.fileUrl);
        }
        onRejected: {
            console.log("Canceled");
        }
    }
}
