import QtQuick 2.3
import QLens.core 0.1

Item {
    id: style

    property color backgroundColor
    property color foregroundColor
    property color textColor
    property color accentColor

    function reloadStyle() {
        textColor = settings.get("theme/textColor", "#B9B9B9")
        backgroundColor = settings.get("theme/backgroundColor", "#272628")
        foregroundColor = settings.get("theme/foregroundColor", "#3B373C")
        accentColor = settings.get("theme/accentsColor", "#1B1B1B")
    }

    Component.onCompleted: {
        reloadStyle()
    }

    SettingsInterface {
        id: settings
    }
}
