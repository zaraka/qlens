#include "opencvpaintitem.h"
#include "imageutils.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <QDebug>


OpenCVPaintItem::OpenCVPaintItem(QQuickItem *parent) : QQuickPaintedItem(parent) {
}

void OpenCVPaintItem::setImage(QImage image){
    m_imageSource = image.copy();
    update();
}

QImage OpenCVPaintItem::getImage() const {
    return m_image;
}

void OpenCVPaintItem::paint(QPainter *painter) {
    if(!m_imageSource.isNull()) {
        m_rt.setMaxDepth(m_manager->maxDepth());
        m_rt.setFov(m_manager->fov());

        painter->eraseRect(QRect(0,0, width(), height()));
        m_rt.setResolution(width(), height());
        std::vector<Object::Material> materials;
        materials.push_back(*m_imageMaterial);

         std::list<Object*> objects;
         objects.push_back(m_triangle1);
         objects.push_back(m_triangle2);


         for(Lens* lens : m_manager->getLensList()) {
             materials.push_back({nullptr, lens->nNormalized()});
             size_t pt = materials.size() - 1;
             Object* newLens;
             if(lens->type() == LensType::CONCAVE){
                newLens = new Biconcave({lens->x(), lens->y(), lens->z()}, lens->r1(), lens->r2(), &materials[pt]);
             } else {
                newLens = new Biconvex({lens->x(), lens->y(), lens->z()}, lens->r1(), lens->r2(), &materials[pt]);
             }
             objects.push_back(newLens);
         }

         const cv::Mat& framebuffer{m_rt.render(objects, m_manager->cameraZ(), m_scale)};

         m_image = qlens::mat2QImage(framebuffer);
         int x = (width() / 2) - (m_image.width() / 2);
         int y = (height() / 2) - (m_image.height() / 2);
         QRect dest(x, y, m_image.width(), m_image.height());

         painter->drawImage(dest, m_image);

         /*for (Object* const o : objects)
             delete o;
         objects.clear();*/
    } else if(m_imageSource.isNull()) {
        painter->eraseRect(QRect());
    }
}

void OpenCVPaintItem::loadNewFile(QVariant filename) {
    QString path = filename.toUrl().toString(QUrl::RemoveScheme);
    QImage image = QImage(path);
    if(image.isNull()){
        qCritical() << "Could not load image " << path;
    }

    if(m_triangle1 != nullptr){
        delete m_triangle1;
    }
    if(m_triangle2 != nullptr){
        delete m_triangle2;
    }
    if(m_imageMaterial != nullptr){
        delete m_imageMaterial;
    }

    m_imageMaterial = new Object::Material{new Texture{path.toStdString()}, 0.f};
    m_triangle1 = new Triangle{{{2.66f, -2.f, -3.f}, {1.f, 0.f}}, {{2.66f, 2.f, -3.f}, {1.f, 1.f}}, {{-2.66f, 2.f, -3.f}, {0.f, 1.f}}, m_imageMaterial};
    m_triangle2 = new Triangle{{{-2.66f, -2.f, -3.f}, {0.f, 0.f}}, {{2.66f, -2.f, -3.f}, {1.f, 0.f}}, {{-2.66f, 2.f, -3.f}, {0.f, 1.f}}, m_imageMaterial};

    setImage(image);
    emit imageLoaded(m_imageSource);
}

void OpenCVPaintItem::saveFile(QVariant filename){
    emit imageSaved();
}

void OpenCVPaintItem::zoom(bool plus){
    if(!m_imageSource.isNull()){
        m_scale += plus ? 0.05 : -0.05;
        update();
    }
}

void OpenCVPaintItem::closeFile(){
    if(!m_imageSource.isNull()){
        m_scale = 0.5f;
        m_image = QImage();
        m_imageSource = QImage();
        update();
        emit imageClosed();
    }
}

void OpenCVPaintItem::setLensManager(LensManager *manager) {
    //connect(minimizeAction, SIGNAL(triggered()), window, SLOT(showMinimized()));
    connect(manager,SIGNAL(lensChanged()),this,SLOT(changed()));
    connect(manager,SIGNAL(wireframeChanged()),this, SLOT(changed()));
    connect(this, SIGNAL(imageClosed()),manager,SLOT(onImageClosed()));
    connect(this, SIGNAL(imageLoaded(QImage)), manager,SLOT(onImageLoaded(QImage)));
    m_manager = manager;
}

void OpenCVPaintItem::changed(){
    update();
}
