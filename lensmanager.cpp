#include "lensmanager.h"
#include <QDebug>

LensManager::LensManager(QObject *parent) : QObject(parent)
{

}

QQmlListProperty<Lens> LensManager::lens() {
    return QQmlListProperty<Lens>(this, m_lens);
}

void LensManager::addLens(){
    if(!m_lock){
        Lens* lens = new Lens(this);
        connect(lens,SIGNAL(changed()),this, SLOT(onLensChanged()));
        m_lens.append(lens);
        emit lensAdded();
        emit lensChanged();
    }
}

void LensManager::deleteLens(int position){
    if(!m_lock) {
        Lens* lens = m_lens.at(position);
        if(lens == m_selectedLens){
            m_selectedLens = nullptr;
        }
        m_lens.removeAt(position);
        emit lensDeleted();
        emit lensChanged();
    }
}

void LensManager::deleteLens(Lens *lensToDelete){
    if(!m_lock) {
        if(lensToDelete == m_selectedLens) {
            m_selectedLens = nullptr;
        }
        m_lens.removeOne(lensToDelete);
        emit lensDeleted();
        emit lensChanged();
    }
}

void LensManager::moveLensUp(Lens *lens){
    if(!m_lock) {
        int index = m_lens.indexOf(lens);
        if(index != -1 && index > 0){
            m_lens.swap(index, index-1);
        }
        emit lensChanged();
    }
}

void LensManager::moveLensDown(Lens *lens){
    if(!m_lock) {
        int index = m_lens.indexOf(lens);
        if(index != -1 && index < m_lens.size() - 1){
            m_lens.swap(index, index+1);
        }
        emit lensChanged();
    }
}

void LensManager::onLensChanged(){
    emit lensChanged();
}

void LensManager::setWireframe(bool wireframe){
    if(m_wireframe != wireframe){
        m_wireframe = wireframe;
        emit wireframeChanged();
    }
}

void LensManager::toggleWireframe(){
    m_wireframe = !m_wireframe;
    emit wireframeChanged();
}

void LensManager::setLock(bool lock){
    if(m_lock != lock){
        m_lock = lock;
        emit lockChanged();
    }
}

void LensManager::onImageLoaded(QImage image){
    setLock(false);
    m_imageRect = QRect(0,0,image.width(), image.height());
    emit imageRectChanged();
}

void LensManager::onImageClosed(){
    m_lens.clear();
    setLock(true);
    emit lensChanged();
}

void LensManager::changeSelected(Lens *lens) {
    if(!m_lock) {
        for(Lens *iLens : m_lens){
            if(lens == iLens){
                m_selectedLens = iLens;
                iLens->setSelected(true);
            } else {
                iLens->setSelected(false);
            }
        }
        emit selectedLensChanged();
        emit lensChanged();
    }
}

void LensManager::setCameraZ(float cameraZ){
    if(m_cameraZ != cameraZ){
        m_cameraZ = cameraZ;
        emit cameraZChanged();
        emit lensChanged();
    }
}

void LensManager::setFov(float fov){
    if(m_fov != fov){
        m_fov = fov;
        emit fovChanged();
        emit lensChanged();
    }
}

void LensManager::setMaxDepth(unsigned int maxDepth){
    if(m_maxDepth != maxDepth){
        m_maxDepth = maxDepth;
        emit maxDepthChanged();
        emit lensChanged();
    }
}
