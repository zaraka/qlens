#ifndef LENS_H
#define LENS_H

#include <QObject>

enum class LensType {
    CONCAVE,
    CONVEX
};

class Lens : public QObject
{
    Q_OBJECT
public:
    explicit Lens(QObject *parent = 0);

    float x() const { return m_x; }
    float y() const { return m_y; }
    float z() const { return m_z; }
    float r1() const { return m_r1; }
    float r2() const { return m_r2; }
    float n() const { return m_n; }
    float nNormalized() const { return m_n + 1.0f; }
    bool selected() const { return m_selected; }
    QString selectedState() const { return (m_selected) ? "selected" : ""; }
    QString typeString() const {
        if(m_type == LensType::CONCAVE){
            return "concave";
        } else {
            return "convex";
        }
    }

    LensType type() const {
        return m_type;
    }

    Q_PROPERTY(float x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(float y READ y WRITE setY NOTIFY yChanged)
    Q_PROPERTY(float z READ z WRITE setZ NOTIFY zChanged)
    Q_PROPERTY(float r1 READ r1 WRITE setR1 NOTIFY r1Changed)
    Q_PROPERTY(float r2 READ r2 WRITE setR2 NOTIFY r2Changed)
    Q_PROPERTY(float n READ n WRITE setN NOTIFY nChanged)
    Q_PROPERTY(bool selected READ selected NOTIFY selectedChanged)
    Q_PROPERTY(QString selectedState READ selectedState NOTIFY selectedChanged)
    Q_PROPERTY(QString type READ typeString WRITE setType NOTIFY typeChanged)
private:
    float m_x = 0.0;
    float m_y = 0.0;
    float m_z = 0.0;
    float m_r1 = 0.0;
    float m_r2 = 0.0;
    float m_n = 1.1;
    bool m_selected = false;
    LensType m_type = LensType::CONVEX;

    LensType stringToType(QString type);

signals:
    void changed();

    void xChanged();
    void yChanged();
    void zChanged();
    void r1Changed();
    void r2Changed();
    void nChanged();
    void selectedChanged();
    void typeChanged();

public slots:
    void setX(float x);
    void setY(float y);
    void setZ(float z);
    void setR1(float r1);
    void setR2(float r2);
    void setN(float n);
    void setSelected(bool selected);
    void setType(QString type);
};

#endif // LENS_H
