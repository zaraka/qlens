import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1
import QLens.core 0.1

Rectangle {
    id: lensView;
    property Rectangle container;
    property LensManager manager;
    property Lens lens;
    property string lensState;

    anchors.left: parent.left
    anchors.right: parent.right
    height: 345
    anchors.margins: 3

    color: style.backgroundColor
    border.width: 2
    border.color: style.accentColor

    SettingsInterface {
        id: settings;
    }

    Style {
        id: style;
    }

    state: lens.selectedState;
    states: [
        State {
            name: "hovered";
            PropertyChanges {target: toolbar; color: Qt.darker(style.accentColor, 1.4); }
        },
        State {
            name: "selected";
            PropertyChanges {target: toolbar; color: Qt.darker(style.accentColor, 1.6); }
        }
    ]



    ColumnLayout {
        anchors.fill: parent;

        Rectangle {
            id: toolbar;
            color: style.accentColor
            anchors.left: parent.left;
            anchors.right: parent.right;
            height: 20;

            function entered(){
                if(lensView.state != "selected"){
                    lensView.state = "hovered";
                }
            }
            function exited(){
                lensView.state = lens.selectedState;
            }

            MouseArea {
                anchors.fill: parent;
                propagateComposedEvents: true;
                hoverEnabled: true;
                acceptedButtons: Qt.LeftButton;
                onClicked: {
                    manager.changeSelected(lens);
                    mouse.accepted = false;
                }
                onEntered: toolbar.entered();
                onExited: toolbar.exited();
            }

            ImageButton {
                id: remove_lens_button
                anchors.top: parent.top
                anchors.right: parent.right
                height: 16
                width: 16
                image: "qrc:/assets/remove.png"
                imageHover: "qrc:/assets/remove_hover.png";
                imageClick: "qrc:/assets/remove_clicked.png";
                color : "transparent";
                colorHover: "transparent";
                colorClick: "transparent";
                anchors.margins: 2
                onClicked: {
                    manager.deleteLens(lens);
                }
                onEntered: toolbar.entered();
                onExited: toolbar.exited();
            }

            ImageButton {
                id: up_lens_button
                anchors.top: parent.top
                anchors.right: remove_lens_button.left
                height: 16
                width: 16
                image: "qrc:/assets/up.png"
                imageHover: "qrc:/assets/up_hover.png";
                imageClick: "qrc:/assets/up_clicked.png";
                color : "transparent";
                colorHover: "transparent";
                colorClick: "transparent";
                anchors.margins: 2
                onClicked: {
                    upAction.trigger();
                }
                onEntered: toolbar.entered();
                onExited: toolbar.exited();
            }

            ImageButton {
                id: down_lens_button
                anchors.top: parent.top
                anchors.right: up_lens_button.left
                height: 16
                width: 16
                image: "qrc:/assets/down.png"
                imageHover: "qrc:/assets/down_hover.png";
                imageClick: "qrc:/assets/down_clicked.png";
                color : "transparent";
                colorHover: "transparent";
                colorClick: "transparent";
                anchors.margins: 2
                onClicked: {
                    downAction.trigger();
                }
                onEntered: toolbar.entered();
                onExited: toolbar.exited();
            }
        }

        GroupBox {
            title: "Type";
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 20;
            anchors.rightMargin: 20;
            RowLayout {
                ExclusiveGroup {
                    id: lensType;
                    onCurrentChanged: lens.type = current.objectName;
                    Component.onCompleted: {
                        current = (lens.type == "convex") ? convex : concave;
                    }
                }
                RadioButton {
                    id: convex;
                    objectName: "convex";
                    text: "Convex";
                    exclusiveGroup: lensType;
                }
                RadioButton {
                    id: concave;
                    objectName: "concave";
                    text: "Concave";
                    exclusiveGroup: lensType;
                }
            }
        }

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 10;
            anchors.rightMargin: 10;
            height: 40;
            color: "transparent";
            Text {
                anchors.top: parent.top
                anchors.right: parent.right;
                anchors.margins: 7;
                id: x_value_text;
                font.pointSize: 8;
                font.family: "Monospace";
                color: style.textColor;
            }

            Slider {
                anchors.top: x_value_text.bottom
                id: x_value_slider;
                minimumValue: -1.33;
                maximumValue: 1.33;
                value: lens.x;
                stepSize: 0.01;
                updateValueWhileDragging: false;
                onValueChanged: {
                    x_value_text.text = "X: " + value;
                    lens.x = value;
                }
                Component.onCompleted: {
                    x_value_text.text = "X: " + value;
                }
            }
        }

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 10;
            anchors.rightMargin: 10;
            height: 40;
            color: "transparent";
            Text {
                anchors.top: parent.top
                anchors.right: parent.right;
                anchors.margins: 7;
                id: y_value_text;
                font.pointSize: 8;
                font.family: "Monospace";
                color: style.textColor;
            }

            Slider {
                anchors.top: y_value_text.bottom
                id: y_value_slider;
                minimumValue: -1.0;
                maximumValue: 1.0;
                value: lens.y;
                stepSize: 0.01;
                updateValueWhileDragging: false;
                onValueChanged: {
                    y_value_text.text = "Y: " + value;
                    lens.y = value
                }
                Component.onCompleted: {
                    y_value_text.text = "Y: " + value;
                }
            }
        }

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 10;
            anchors.rightMargin: 10;
            height: 40;
            color: "transparent";
            Text {
                anchors.top: parent.top
                anchors.right: parent.right;
                anchors.margins: 7;
                id: z_value_text;
                font.pointSize: 8;
                font.family: "Monospace";
                color: style.textColor;
            }

            Slider {
                anchors.top: z_value_text.bottom
                id: z_value_slider;
                minimumValue: -5.0;
                maximumValue: 5.0;
                value: lens.z;
                stepSize: 0.1;
                updateValueWhileDragging: false;
                onValueChanged: {
                    z_value_text.text = "Z: " + value;
                    lens.z = value
                }
                Component.onCompleted: {
                    z_value_text.text = "Z: " + value;
                }
            }
        }

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 10;
            anchors.rightMargin: 10;
            height: 40;
            color: "transparent";
            Text {
                anchors.top: parent.top
                anchors.right: parent.right;
                anchors.margins: 7;
                id: r1_value_text;
                font.pointSize: 8;
                font.family: "Monospace";
                color: style.textColor;
            }

            Slider {
                anchors.top: r1_value_text.bottom
                id: r1_value_slider;
                minimumValue: 0.0;
                maximumValue: 1.0;
                value: lens.r1;
                stepSize: 0.01;
                updateValueWhileDragging: false;
                onValueChanged: {
                    r1_value_text.text = "r1: " + value;
                    lens.r1 = value
                }
                Component.onCompleted: {
                    r1_value_text.text = "r1: " + value;
                }
            }
        }

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 10;
            anchors.rightMargin: 10;
            height: 40;
            color: "transparent";
            Text {
                anchors.top: parent.top
                anchors.right: parent.right;
                anchors.margins: 7;
                id: r2_value_text;
                font.pointSize: 8;
                font.family: "Monospace";
                color: style.textColor;
            }

            Slider {
                anchors.top: r2_value_text.bottom
                id: r2_value_slider;
                minimumValue: 0.0;
                maximumValue: 1.0;
                value: lens.r2;
                stepSize: 0.01;
                updateValueWhileDragging: false;
                onValueChanged: {
                    r2_value_text.text = "r2: " + value;
                    lens.r2 = value
                }
                Component.onCompleted: {
                    r2_value_text.text = "r2: " + value;
                }
            }
        }

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 10;
            anchors.rightMargin: 10;
            height: 40;
            color: "transparent";
            Text {
                anchors.top: parent.top
                anchors.right: parent.right;
                anchors.margins: 7;
                id: n_value_text;
                font.pointSize: 8;
                font.family: "Monospace";
                color: style.textColor;
            }

            Slider {
                anchors.top: n_value_text.bottom
                id: n_value_slider;
                minimumValue: 0.0;
                maximumValue: 1.0;
                value: lens.n;
                stepSize: 0.01;
                updateValueWhileDragging: false;
                onValueChanged: {
                    var showValue = value + 1.0;
                    n_value_text.text = "N: " + showValue;
                    lens.n = value
                }
                Component.onCompleted: {
                    var showValue = value + 1.0;
                    n_value_text.text = "N: " + showValue;
                }
            }
        }

    }

    Menu {
        id: contextMenu;
        title: qsTr("Menu");

        MenuItem {
            text: qsTr("Delete");
            onTriggered: deleteLens();
        }

    }
}
