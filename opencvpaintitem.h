#ifndef OPENCVPAINTITEM_H
#define OPENCVPAINTITEM_H

#include "lensmanager.h"

#include <QQuickPaintedItem>
#include <QPainter>

//backend
#include "backend/include/ray_tracer.h"
#include "backend/include/texture.h"
#include "backend/include/object.h"
#include "backend/include/triangle.h"
#include "backend/include/biconvex.h"
#include "backend/include/biconcave.h"

class OpenCVPaintItem : public QQuickPaintedItem
{
    Q_OBJECT
public:
    OpenCVPaintItem(QQuickItem *parent = 0);
    void paint(QPainter *painter);

    void setImage(QImage image);
    QImage getImage() const;
    void setLensManager(LensManager* manager);

    Q_INVOKABLE void loadNewFile(QVariant filename);
    Q_INVOKABLE void saveFile(QVariant filename);
    Q_INVOKABLE void zoom(bool plus);
    Q_INVOKABLE void closeFile();

signals:
    void imageLoaded(QImage image);
    void imageSaved();
    void imageClosed();

public slots:
    void changed();


private:
    LensManager* m_manager;

    QImage m_image; //painted image, this image can be transformed!
    QImage m_imageSource; //source image, non transformed

    float m_scale = 0.5f;
    int m_x = 0;
    int m_y = 0;

    RayTracer m_rt;

    Triangle* m_triangle1 = nullptr;
    Triangle* m_triangle2 = nullptr;
    Object::Material* m_imageMaterial = nullptr;

};

#endif // OPENCVPAINTITEM_H
