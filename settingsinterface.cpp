#include "settingsinterface.h"

#include <QDebug>

using namespace std;

SettingsInterface::SettingsInterface(QObject *parent) :
    QObject(parent) , m_settings(QSettings::UserScope, "fitstud","QLens", parent)
{
}

QVariant SettingsInterface::get(QVariant key, QVariant defVal){
    if(!m_settings.contains(key.toString())){
        set(key.toString(), defVal);
    }
    QVariant rets = m_settings.value(key.toString(), defVal);
    if(rets.type() == QVariant::String && (rets.toString() == "true" || rets.toString() == "false"))
        return rets.toBool();

    //qWarning() << "get " << key.toString() << " " << rets;
    return rets;
}

void SettingsInterface::set(QVariant key, QVariant value){
    m_settings.setValue(key.toString(), value);
    m_settings.sync();
    emit change();
}
