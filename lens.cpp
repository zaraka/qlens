#include "lens.h"

#include <QDebug>

Lens::Lens(QObject *parent) : QObject(parent)
{

}

void Lens::setX(float x){
    if(x != m_x){
        m_x = x;
        emit xChanged();
        emit changed();
    }
}

void Lens::setY(float y){
    if(y != m_y){
        m_y = y;
        emit yChanged();
        emit changed();
    }
}

void Lens::setZ(float z){
    if(z != m_z){
        m_z = z;
        emit zChanged();
        emit changed();
    }
}


void Lens::setR1(float r1){
    if(r1 != m_r1){
        m_r1 = r1;
        emit r1Changed();
        emit changed();
    }

}

void Lens::setR2(float r2){
    if(r2 != m_r2){
        m_r2 = r2;
        emit r2Changed();
        emit changed();
    }
}

void Lens::setN(float n){
    if(n != m_n){
        m_n = n;
        emit nChanged();
        emit changed();
    }
}


void Lens::setSelected(bool selected){
    m_selected = selected;
    emit selectedChanged();
    emit changed();
}

void Lens::setType(QString type){
    type = type.toLower();
    if(type == "concave" || type == "convex"){
        LensType t = (type == "concave") ? LensType::CONCAVE : LensType::CONVEX;
        if(m_type != t){
            m_type = t;
            emit typeChanged();
            emit changed();
        }
    }
}
