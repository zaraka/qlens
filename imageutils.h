#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H

#include <QImage>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace qlens {
    inline QImage mat2QImage(cv::Mat const& src) {
        cv::Mat temp;
        cvtColor(src, temp,CV_BGR2RGB);
        QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
        dest.bits();
        return dest;
    }

    cv::Mat qImage2Mat(QImage const& src) {
         cv::Mat tmp(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine());
         cv::Mat result;
         cvtColor(tmp, result,CV_BGR2RGB);
         return result;
    }
}

#endif // IMAGEUTILS_H
