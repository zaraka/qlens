import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2
import QLens.core 0.1

Window {
    property LensManager manager;
    id: window;

    SettingsInterface {
        id: settings;
    }

    Style {
        id: style;
    }

    title: qsTr("Settings");
    width: 300;
    height: 150;
    maximumHeight: window.height;
    maximumWidth: window.width;
    minimumHeight: window.height;
    minimumWidth: window.width;
    color: style.foregroundColor;

    ColumnLayout {
        anchors.fill: parent;

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 10;
            anchors.rightMargin: 10;
            height: 40;
            color: "transparent";
            Text {
                anchors.top: parent.top
                anchors.right: parent.right;
                anchors.margins: 7;
                id: maxDepth_value_text;
                font.pointSize: 8;
                font.family: "Monospace";
                color: style.textColor;
            }

            Slider {
                anchors.top: maxDepth_value_text.bottom;
                id: maxDepth_value_slider;
                minimumValue: 0.0;
                maximumValue: 20.0;
                value: manager.maxDepth;
                stepSize: 1.0;
                updateValueWhileDragging: false;
                onValueChanged: {
                    maxDepth_value_text.text = "Max depth: " + value;
                    manager.maxDepth = value;                }
                Component.onCompleted: {
                    maxDepth_value_text.text = "Max depth: " + value;
                }
            }
        }

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 10;
            anchors.rightMargin: 10;
            height: 40;
            color: "transparent";
            Text {
                anchors.top: parent.top
                anchors.right: parent.right;
                anchors.margins: 7;
                id: fov_value_text;
                font.pointSize: 8;
                font.family: "Monospace";
                color: style.textColor;
            }

            Slider {
                anchors.top: fov_value_text.bottom;
                id: fov_value_slider;
                minimumValue: 0.0;
                maximumValue: 120.0;
                value: manager.fov;
                stepSize: 1.0;
                updateValueWhileDragging: false;
                onValueChanged: {
                    fov_value_text.text = "FOV: " + value + "°";
                    manager.fov = value;
                }
                Component.onCompleted: {
                    fov_value_text.text = "FOV: " + value + "°";
                }
            }
        }

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.leftMargin: 10;
            anchors.rightMargin: 10;
            height: 40;
            color: "transparent";
            Text {
                anchors.top: parent.top
                anchors.right: parent.right;
                anchors.margins: 7;
                id: z_value_text;
                font.pointSize: 8;
                font.family: "Monospace";
                color: style.textColor;
            }

            Slider {
                anchors.top: z_value_text.bottom;
                id: z_value_slider;
                minimumValue: 0.0;
                maximumValue: 8.0;
                value: manager.cameraZ;
                stepSize: 0.1;
                updateValueWhileDragging: false;
                onValueChanged: {
                    z_value_text.text = "Camera Z: " + value;
                    manager.cameraZ = value;
                }
                Component.onCompleted: {
                    z_value_text.text = "Camera Z: " + value;
                }
            }
        }
    }

}
