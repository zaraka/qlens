TEMPLATE = app

QT += qml quick widgets gui core

CONFIG += c++11 console
QMAKE_CXXFLAGS_RELEASE *= -O3

SOURCES += main.cpp \
    settingsinterface.cpp \
    qlensinterface.cpp \
    opencvpaintitem.cpp \
    lensmanager.cpp \
    lens.cpp \
    backend/src/biconcave.cc \
    backend/src/biconvex.cc \
    backend/src/ray.cc \
    backend/src/ray_tracer.cc \
    backend/src/sphere.cc \
    backend/src/texture.cc \
    backend/src/triangle.cc

RESOURCES += qml.qrc \
    resource.qrc

#Does this work?
LIBS += `pkg-config --libs opencv`


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    settingsinterface.h \
    qlensinterface.h \
    opencvpaintitem.h \
    imageutils.h \
    lensmanager.h \
    lens.h \
    backend/include/biconcave.h \
    backend/include/biconvex.h \
    backend/include/object.h \
    backend/include/ray.h \
    backend/include/ray_tracer.h \
    backend/include/sphere.h \
    backend/include/texture.h \
    backend/include/triangle.h

DISTFILES +=
