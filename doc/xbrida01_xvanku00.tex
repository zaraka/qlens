\documentclass[11pt,a4paper,titlepage]{article}

\usepackage[left=2cm,text={17cm,24cm},top=3cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{url}
\usepackage{hyperref}

%\DeclareUrlCommand\url{\def\UrlLeft{<}\def\UrlRight{>} \urlstyle{tt}}
\providecommand{\uv}[1]{\quotedblbase #1\textquotedblleft}
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}
\hbadness 5000

\begin{document}
	\begin{titlepage}
		\begin{center}
			\Huge{\textsc{Vysoké učení technické v~Brně}} \\
			\huge{\textsc{Fakulta informačních technologií}} \\
			\vspace{\stretch{0.382}}
			\LARGE{Zpracování obrazu\,--\,semestrální projekt} \\
			\Huge{\#54: Změna obrazu přiložením čočky} \\
			\vspace{\stretch{0.618}}
		\end{center}

		\Large{\today \hfill Ján\,Brída, Nikita\,Vaňků}
	\end{titlepage}

	\newpage
	\section{Zadání}
	Cílem projektu bylo vytvořit aplikaci s~grafickým rozhraním, která uživateli umožňuje provádět a~zobrazovat změnu obrazu přiložením čočky, resp. soustavou čoček.
	Uživateli mělo být umožněno nastavovat pozici obrazu (relativně k~pozorovateli), aplikovaných čoček a~jejich parametry, to jest poloměry křivosti sfér, tvořící danou čočku
	a~index lomu. Dalším důležitým požadavkem bylo umožnit přiložení různých typů čoček, konkrétně spojka a~rozptylka.

	\subsection{Barrel efekt}
	K~tomuto jevu dochází při použití konkávních povrchů čoček. Jak je vidět na~obrázku~\ref{fig:barrel}, dochází k~zmenšení a~\uv{zakulacení} výsledku.

	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=0.3]{images/barrel.png}
		\caption{Barrel efekt.}
		\label{fig:barrel}
	\end{figure}

	\subsection{Pin-cushion efekt}
	Opakem barrel efektu je typ pin-cushion. Je pozorovatelný u~spojek~\ref{fig:pincushion}, nejvíce u~bikonvexních. Doprovázející charakteristikou je zvětšení obrazu a~lze
	tento typ čočky najít například v~lupě.

	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=0.3]{images/pincushion.png}
		\caption{Pin-cushion efekt.}
		\label{fig:pincushion}
	\end{figure}

	\section{Implementační detaily}
	Při implementaci byl použit algoritmus Ray Tracingu (RT), který umožňuje dle zasaženého průhledného povrchu pomocí optických zákonů odrazu~\ref{sub:refl} a~lomu~\ref{sub:refr} světla
	vytvořit odpovídající výsledný obraz. RT funguje na principu zjištění průsečíku $\vec{p}$ sledovaného paprsku podle
	\begin{equation}
		\vec{p}(t) = \vec{o} + t \: \vec{d}
		\label{eq:ray}
	\end{equation}
	kde $\vec{o}$ je pozice, ze které paprsek vychází, $\vec{d}$ je jednotkový vektor určující směr šíření parpsku a~$t$ je parametr. Na základě funkcí jednotlivých implicitních ploch tvaru
	$F(x, y, z) = 0$, jako je např. sféra, lze dosadit~\eqref{eq:ray} do těchto rovnic a~spočítat daný bod $\vec{p}(t)$ po zjištění $t$.~\cite{pgr}

	\subsection{Trojúhelník, textura, UV souřadnice a~bilineární interpolace} \label{sub:tri}
	Průsečík paprsku s~trojúhelníkem $\vec{p}(u, v)$, definován body $\vec{a}$, $\vec{b}$, $\vec{c}$, je dán pomocí barycentrických souřadnic $(u, v)$
	\begin{equation}
		\vec{p}(u, v) = (1 - u~- v) \: \vec{a} + u~\: \vec{b} + v~\: \vec{c}
		\label{eq:bary}
	\end{equation}
	Platí, že $u \geq 0$, $v \geq 0$ a~$u + v~\leq 1$. Po dosazení parametrické rovnice pro polopřímku:
	$$
		\left[ -\vec{d}, \quad \vec{b} - \vec{a}, \quad \vec{c} - \vec{a}\right]
		\begin{bmatrix}
			t \\
			u~\\
			v~\\
		\end{bmatrix}
		= \vec{o} - \vec{a}
	$$
	Geometricky se v~podstatě jedná o~translaci trojúhelníku do počátku souřadného systému s~následnou transformací na hrany s~jednotkovou délkou na $y$- a~$z$-ové ose,
	s~paprskem rovnoběžným k~ose $x$. Pomocí Cramerova pravidla se lze ze soustavy výše dopočítat k~průsečíku $\vec{p}$. Translace nemění nic na tom, jak se pracuje s~barycentrickými
	souřadnicemi, tudíž tuto transformaci s~klidem použít, aby se s~trojúhelníkem lépe pracovalo.~\cite{mt}

	V~projektu došlo na použití trojúhelníků při sestavení plátna pro zvolený obrázek. Na základě vypočtených barycentrických souřadnic se poté dopracuje k~texturovacím souřadnicím
	podle příslušných UV pozicím asociovaným k~vrcholům trojúhelníka (podobně jako~\eqref{eq:bary}). UV souřadnice $(u, v)$ souvisí s~texturovacím souřadným systémem, kterým je dvoudimenzionální
	kartézský souřadný systém používající normalizované souřadnice, t.j. v~intervalu $[0 .. 1]$.

	Při vzorkování textury dochází k~aliasingu. Proto bývá použit nějaký způsob filtrace textury pro zkrášlení výsledku. Existuje vícero způsobů aplikace anti-aliasingu (mip-mapping, anizotropní filtrování),
	v~našem případě byla použita běžná bilineární interpolace. Necelá část $\theta = u~- i$ a~$\phi = v~- j$ udává vzdálenost od texelu na pozici $(i, j)$. Potom lze bilineární operaci popsat následovně:
	\begin{align*}
		f(u, j) & = (1 - \theta) \: f(i, j) + \theta \: f(i + 1, j) \\
		f(u, j + 1) & = (1 - \theta) \: f(i, j + 1) + \theta \: f(i + 1, j + 1) \\
		f(u, v) & = (1 - \phi) \: f(u, j) + \phi \: f(u, j + 1) \numberthis
	\end{align*}
	$f$ je vzorkovací funkce. Názorněji viz~\ref{fig:bilinear}.

	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=0.3]{images/bilinear.png}
		\caption{Bilineární interpolace. \href{http://www.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/43533/versions/2/screenshot.png}{zdroj}}
		\label{fig:bilinear}
	\end{figure}

	\subsection{Sféra, Constructive Solid Geometry operace} \label{sub:sphere}
	Sféra je důležitým stavebním kamenem při implementaci čočky. Pamatujeme si, že paprsek je vyjádřen jako~\eqref{eq:ray}. Sférický objekt může být definován jako implicitní plocha
	\begin{align*}
		x^2 + y^2 + z^2 & = R^2 \\
		x^2 + y^2 + z^2 - R^2 & = 0 \numberthis \label{eq:sphere} \\
	\end{align*}
	kde $R$ je poloměr sféry. Pro body na tomto povrchu musí platit~\eqref{eq:sphere}. Dosazením~\eqref{eq:ray} do~\eqref{eq:sphere} dostaneme
	\begin{equation}
		\vec{o}^2 + (\vec{d} \: t)^2 + 2 \: \vec{o} \: \vec{d} \: t - R^2 = \vec{o}^2 + \vec{d}^2 \: t^2 + 2 \: \vec{o} \: \vec{d} \: t - R^2 \: 0
		\label{eq:sphere2}
	\end{equation}
	což je kvadratická rovnice, jejímiž kořeny jsou $t_1, t_2$, kde $t_1 \leq t_2$, a~podle toho, zda-li se $\vec{o}$ nachází uvnitř sféry, mohou nabývat i~záporné hodnoty~\ref{fig:sphere}.

	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=2.5]{images/sphere.png}
		\caption{Případy průsečíku paprsku se sférou. \href{http://www.scratchapixel.com/images/upload/ray-simple-shapes/rayspherecases.png?}{zdroj}}
		\label{fig:sphere}
	\end{figure}

	Sestavení čoček se nakonec provádí pomocí Constructive Solid Geometry (CSG) operací průniku a~rozdílu. CSG je metoda vytváření komplikovaných tvarů v~RT skládáním implicitních ploch
	s~množinovými operacemi. Pomocí intervalové aritmetky lze jednoduše pracovat nad intervaly parametru $t$ při protnutí parpsku s~objektem. U~bikonvexní čočky proběhlo sestavení za pomocí dvou sfér $A$, $B$.
	Průnik $A \cap B$ vytváří spojku a~je definován tak, že pro intervaly $[t^A_1, t^A_2]$ a~$[t^B_1, t^B_2]$ jsou vypočteny $t_a = \max(t^A_1, t^B_1)$ spolu s~$t_b = \min(t^A_2, t^B_2)$. Pokud $t_a > t_b$, potom
	nedošlo k~průniku, jinak je interval $[t_a, t_b]$ výsledkem.

	U~rozptylky (bikonkávní čočky) je to trochu složitější. Pro sestavení se použije množinová operace rozdílu $\setminus$ nad sférami $A$, $B$ a~$C$ následovně: $(C \setminus A) \setminus B$. Jednoduše se dá implementovat
	$A \setminus B$ pro libovolné sféry $A$ a~$B$ jako $A \cap \cdot B$, kde $\cdot B$ je komplement $B$. V~RT se za komplement považuje interval mimo rozsahu průsečíků. Tudíž pokud $[t^B_1 .. t^B_2]$, je komplement
	$\cdot B = \left\{[-\infty .. t^B_1], [t^B_2 .. \infty]\right\}$
	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=0.2]{images/biconvex.png}
		\caption{Sestavení bikonvexní čočky.}
		\label{fig:biconvex}
	\end{figure}

	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=0.2]{images/biconcave.png}
		\caption{Sestavení bikonkávní čočky.}
		\label{fig:biconcave}
	\end{figure}

	\subsection{Lom světla} \label{sub:refr}
	Když se světlo šíří z~jednoho prostředí do druhého, změní svůj směrový vektor. Faktory, které ovlivňují tento ohyb jsou úhel $\theta_i$ a~index lomu $\eta$.
	Refrakce se řídí Snellovým zákonem, který říká, že poměr sínů úhlů dopadu $\theta_i$ a~lomu $\theta_t$ páru prostředí je roven poměru indexů lomu $\eta_t$ prostředí, ve kterém se paprsek
	láme a~$\eta_i$ prostředí, ze kterého vychází~\eqref{eq:snell}.~\cite{shading}
	\begin{equation}
		\frac{\sin(\theta_i)}{\sin(\theta_t)} = \frac{\eta_t}{\eta_i}
		\label{eq:snell}
	\end{equation}

	Podobně jako v~\ref{sub:refl} budeme i~zde uvažovat $\vec{i}$ a~$\vec{n}$. Je definován vektor $\vec{t}$, popisující směr šíření propuštěného světla na druhé straně roviny
	dopadu, a~je produktem vektorů $\vec{a}$ a~$\vec{b}$ (viz obrázek~\ref{fig:refr}). Platí, že
	\begin{align*}
		\vec{a} & = \vec{m} \: \sin(\theta_t) \\
		\vec{b} & = -\vec{n} \: \sin(\theta_t)
	\end{align*}
	kde~$\vec{m}$ je určen
	$$
		\vec{m} = \frac{\vec{i} + \vec{c}}{\sin(\theta_i)}
	$$
	ve vztahu k~$\vec{c} = \cos(\theta_i) \: \vec{n}$. $\vec{t}$ potom můžeme po dosazení a~za použití Snellova zákona odvodit jako
	\begin{equation}
		\vec{t} = \frac{\eta_i}{\eta_t} \: (\vec{i} + \cos(\theta_i) \: \vec{n}) - \vec{n} \: \cos(\theta_t)
		\label{eq:t1}
	\end{equation}
	Z~trigoniometrických vztahů je známo, že $\cos^2(\theta) + \sin^2(\theta) = 1$ a~tedy $\cos(\theta) = \sqrt{1 - \sin^2(\theta)}$. Potom je výhodné přepsat~\eqref{eq:t1} na
	\begin{equation}
		\vec{t} = \eta \: \vec{i} + (\eta \: c_1 - c_2) \: \vec{n}
		\label{eq:t}
	\end{equation}
	kde
	\begin{align*}
		\eta & = \frac{\eta_i}{\eta_t} \\
		c_1 & = \cos(\theta_i) = \vec{i} \cdot \vec{n} \\
		c_2 & = \sqrt{1 - \eta^2 \: (1 - \cos^2(\theta_i))}
	\end{align*}

	\subsubsection{Totální odraz}
	Když je úhel dopadu větší než tzv. kritický úhel, nastává fenomén totálního odrazu\,--\,100 \% dopadajícího světla se odrazí (neuvažujeme absorpci).
	K~tomuto jevu dochází pouze v~případě, je-li $\eta_i > \eta_t$, např. voda $\rightarrow$ vzduch. Kritický úhel $\theta_c$ lze spočítat ze Snellova zákona
	následovně
	\begin{align*}
		\eta_i \: \sin(\theta_c) & = \eta_t \: \sin\left(\frac{\pi}{2}\right) \\
		\theta_c & =_{\eta_i > \eta_t} \arcsin\left(\frac{\eta_t}{\eta_i}\right) \numberthis \label{eq:crit}
	\end{align*}
	protože v~případě totálního odrazu musí být $\theta_t$ přesně $\frac{\pi}{2}$.

	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=0.12]{images/refr.png}
		\caption{Lom světla.}
		\label{fig:refr}
	\end{figure}

	\subsection{Odraz světla} \label{sub:refl}
	Výsledkem odrazu světla je, co se stane s~fotonem po dopadě na reflektivní povrch, např. skla nebo vody. To co se děje je velmi podobné dopadu
	fotbalového míče na zem\,--\,odrazí se ve směru symetrickém s~vektorem dopadu dle normály povrchu. Zákon odrazu tedy říká
	\begin{equation}
		\theta_r = -\theta_i
	\end{equation}
	kde $\theta_r$ je úhel odrazu a~$\theta_i$ úhel dopadu.

	Je-li nám znám směr dopadajícího paprsku $\vec{i}$ a~normála $\vec{n}$ zasaženého povrchu, dá se vektor odrazu $\vec{r}$ snadno spočítat.~\cite{shading} $\vec{i}$
	a~$\vec{r}$ možno vyjádřit jako
	\begin{align*}
		\vec{i} & = \vec{a} + \vec{b} \\
		\vec{r} & = \vec{a} - \vec{b} \numberthis \label{eq:r1}
	\end{align*}
	viz obrázek~\ref{fig:refl}. Projekcí $\vec{i}$ (anebo $\vec{r}$) na $\vec{n}$ lze získat $\vec{b}$:
	$$
		\vec{b} = \cos(\theta) \: \vec{n}
	$$
	Protože $\vec{i}$ a~$\vec{n}$ by měly být normalizované vektory, $\cos(\theta_i)$ lze přepsat na skalární součin $\vec{i} \cdot \vec{n}$.
	Díky tomu lze odvodit, že
	$$
		\vec{a} = \vec{i} - \cos(\theta) \: \vec{n}
	$$
	a následně po dosazení do~\eqref{eq:r1}:
	\begin{equation}
		\vec{r} = \vec{i} - 2 \: (\vec{i} \cdot \vec{n}) \: \vec{n}
		\label{eq:r}
	\end{equation}

	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=0.13]{images/refl.png}
		\caption{Odraz světla.}
		\label{fig:refl}
	\end{figure}

	\subsection{Uživatelské rozhraní}
	Uživatelské rozhraní (dále jen \texttt{GUI}) je vytvořeno pomocí frameworku \texttt{Qt} a jeho nadstavby \texttt{QML}. Okno aplikace je rozděleno do dvou částí:
	\begin{itemize}
	 \item Postranní panel čoček
	 \item Vykreslovací plátno pro \texttt{OpenCV}
	\end{itemize}
	Zároveň je aplikace doplněna proužkem menu pomocí kterého může uživatel nahrávat obrázkové soubory, uzavírat je či zobrazit okno s~dalším nastavením. Okno aplikace lze vidět na obrázku \ref{fig:app}.
	
	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=0.5]{images/gui1.png}
		\caption{Hlavní okno aplikace.}
		\label{fig:app}
	\end{figure}
	
	V~horní části levého panelu se nachází proužek s~tlačítky. První tlačítko s~ikonou plus přidá novou čočku do scény, lze také využít  klávesu \texttt{l}.
	Každou čočku lze modifikovat pomocí několika ovládacích elementů. Lze modifikovat typ čočky (spojka či rozptylka), její pozice ve scéně, poloměry křivosti a index lomu čočky. Při každé změně těchto parametrů, aplikace překreslí scénu.
	Uživatel může vložit libovolné množství čoček, každá nově vytvořená čočka je přidána na konec seznamu. Čočky lze smazat kliknutím ikony s~křížkem, či označením čočky a stisknutí klávesy \texttt{delete}. 
	
	\begin{figure}[!ht]
		\centering
		\includegraphics[scale=0.65]{images/gui2.png}
		\caption{Okno s~dalším nastavením.}
		\label{fig:options}
	\end{figure}
	V~okně s~dalším nastavením (obrázek \ref{fig:options}) uživatel může modifikovat údaje vzdálenost kamery od snímku, uhel zorného pole a maximální hloubku paprsků RT.
	
	\section{Rozdělení práce v~týmu}
	Za zdokumentování a~implementaci jsou v~týmu zodpovědní následovně:
	\begin{itemize}
		\item Ján Brída (xbrida01)\,--\,odraz a~lom světla, sféra a~skládání CSG operací,
		\item Nikita Vaňků (xvanku00)\,--\,uživatelské rozhraní, trojúhelník a~bilineární filtrace.
	\end{itemize}

	\newpage
	\bibliographystyle{czechiso}
	\bibliography{xbrida01_xvanku00}
\end{document}
