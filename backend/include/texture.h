#ifndef TEXTURE_H
#define TEXTURE_H

#include <opencv2/highgui/highgui.hpp> // imread je zde??? WTF!!!!!
#include <glm/detail/type_vec2.hpp>
#include <glm/detail/type_vec3.hpp>

class Texture {
	cv::Mat m;

	public:
	Texture(const std::string filename) : m{cv::imread(filename, CV_LOAD_IMAGE_COLOR)} {}
	Texture(const cv::Mat& m) : m{m.clone()} {}

	const glm::vec3 sample(const glm::vec2&);
};

#endif
