#ifndef RAY_TRACER_H
#define RAY_TRACER_H

#include <vector>
#include <list>

#include <opencv2/core/core.hpp>
#include <glm/detail/type_vec3.hpp>

#include "ray.h"

class Object;

class RayTracer {
	int width, height;
	float aspectRatio, fov;
	cv::Mat framebuffer;
	unsigned maxDepth;

	static cv::Vec3f glm2Cv(const glm::vec3& v) { return {v.b, v.g, v.r}; }
	glm::vec3 trace(const Ray, const std::list<Object*>&, const unsigned = 0);

	public:
    RayTracer() {}
    RayTracer(const int, const int, const float, const unsigned);

	void setMaxDepth(const unsigned maxDepth) { this->maxDepth = maxDepth; }
    void setFov(const float fov) { this->fov = fov; }
    void setResolution(const int width, const int height) { this->width = width; this->height = height; }

    const cv::Mat& render(const std::list<Object*>&, const float, const float = .5f);
};

inline RayTracer::RayTracer(const int width, const int height, const float fov, const unsigned maxDepth) : width{width}, height{height}, fov{fov},
                                                                                                           maxDepth{maxDepth}
{
}

#endif
