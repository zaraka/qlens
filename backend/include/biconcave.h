#ifndef BICONCAVE_H 
#define BICONCAVE_H

#include "object.h"
#include "sphere.h"

/*   _____________
 *  /   /\:::/\   \
 * | a |::|c|::| b |
 *  \___\/:::\/___/
 */
class Biconcave : public Object {
	Sphere a, b, c; // C MUST be in between the spheres A and B

	public:
	Biconcave(const Sphere a, const Sphere b, const Sphere c) : a{a}, b{b}, c{c} {}
	Biconcave(const glm::vec3, const float, const float, const Material* const);

	Object::Intersection intersect(const Ray&) const;
};

#endif
