#ifndef OBJECT_H
#define OBJECT_H

#include <vector>

#include <glm/detail/type_vec3.hpp>
#include <glm/detail/type_vec2.hpp>

class Texture;
struct Ray;

class Object {
	public:
	struct Material {
		Texture* const bitmap;
		const float eta;
	};

	struct Intersection {
		float t;
		glm::vec3 p, n; // hit point, normal
		glm::vec2 uv;
		const Material* s;
	};

	struct Interval {
		enum { X, Y, SIZE };

		struct Pair {
			float t;
			const Object* o;
			bool invertNormal;
		};

		std::vector<Pair> l;

		Interval operator&(const Interval&) const; // intersection
		Interval operator-(const Interval&) const; // difference (using intersection and complement)

		private:
		static Pair min(const Pair& a, const Pair& b) { return a.t < b.t ? a : b; }
		static Pair max(const Pair& a, const Pair& b) { return a.t > b.t ? a : b; }
	};

	Object() {}
	Object(const Material* const s) : s{s} {} 
    virtual ~Object() {}

	virtual Intersection intersect(const Ray&) const = 0;

	const Material* getSurface() const { return s; }

	protected:
	const Material* s;
};

inline Object::Interval Object::Interval::operator&(const Object::Interval& other) const
{
	const Pair a{max(l[X], other.l[X])},
	           b{min(l[Y], other.l[Y])};

	if (a.t > b.t)
		return {};
	return {{a, b}};
}

inline Object::Interval Object::Interval::operator-(const Object::Interval& other) const
{
	const float inf{100000.f};
	const Interval a{*this & Interval{{{-inf, nullptr, false}, {other.l[X].t, other.l[X].o, true}}}},
	               b{*this & Interval{{{other.l[Y].t, other.l[Y].o, true}, {inf, nullptr, false}}}};
	if (a.l.empty()) {
		if (b.l.empty())
			return {};
		return b;
	}
	return a;
}

#endif
