#ifndef SPHERE_H
#define SPHERE_H

#include <list>

#include "object.h"

struct Sphere : public Object {
	glm::vec3 c; // center
	float r; // radius

    Sphere() {}
    Sphere(const Sphere&);
	Sphere(const glm::vec3 c, const float r, const Object::Material* const s) : Object{s}, c{c}, r{r} {}

    Sphere& operator=(const Sphere&);

	Object::Intersection intersect(const Ray&) const;
	//void intersect(const Ray&, Object::Intersection&, Object::Intersection&) const;
	std::list<Interval> intersectAll(const Ray&) const;

	private:
	static bool solveQuadratic(const float, const float, const float, float&, float&);
};

inline Sphere::Sphere(const Sphere& other) : Object{other.s}, c{other.c}, r{other.r}
{
}

inline Sphere& Sphere::operator=(const Sphere& other)
{
    c = other.c;
    r = other.r;
    s = other.s;
    return *this;
}

#endif
