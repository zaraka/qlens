#ifndef BICONVEX_H
#define BICONVEX_H

#include "object.h"
#include "sphere.h"

/*   _______
 *  /   /\   \
 * | a |::| b |
 *  \___\/___/
 */
class Biconvex : public Object {
	Sphere a, b;

	public:
	Biconvex(const Sphere a, const Sphere b) : a{a}, b{b} {}
	Biconvex(const glm::vec3, const float, const float, const Material* const);

	Object::Intersection intersect(const Ray&) const;
};

inline Biconvex::Biconvex(const glm::vec3 pos, const float r1, const float r2, const Material* const s)
{
	a = Sphere{pos, r1, s};
	b = Sphere{{pos.x, pos.y, pos.z - r2}, r2, s};
}

#endif
