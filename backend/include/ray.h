#ifndef RAY_H
#define RAY_H

#include <glm/geometric.hpp>

struct Ray {
	glm::vec3 o, d;

	Ray(const glm::vec3 o, const glm::vec3 d) : o{o}, d{glm::normalize(d)} {}

	static float getEps() { return eps; }
	Ray castReflective(const glm::vec3&, const glm::vec3&) const;
	Ray castRefractive(const glm::vec3&, const glm::vec3&, float) const;

	private:
	static const float eps;
};

#endif
