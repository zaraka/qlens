#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "object.h"

class Triangle : public Object {
	public:
	struct Vertex {
		glm::vec3 pos;
		glm::vec2 uv;
	};

	Triangle(const Vertex a, const Vertex b, const Vertex c, const Object::Material* const s) : Object{s}, a{a}, b{b}, c{c} {}

	Object::Intersection intersect(const Ray&) const;

	private:
	Vertex a, b, c;
};

#endif
