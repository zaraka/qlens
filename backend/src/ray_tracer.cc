#include <glm/trigonometric.hpp>
#include <glm/detail/type_mat4x4.hpp>

#include "backend/include/ray_tracer.h"
#include "backend/include/object.h"
#include "backend/include/texture.h"

const cv::Mat& RayTracer::render(const std::list<Object*>& objects, const float eyeZ, const float zoom)
{
    framebuffer = cv::Mat{height, width, CV_32FC3};
    aspectRatio = static_cast<float>(width) / static_cast<float>(height);
    glm::mat4 camera2World;
	const float scale(glm::tan(glm::radians<float>(fov * zoom)));
    const glm::vec3 o{(camera2World * glm::vec4{glm::vec3{0.f, 0.f, eyeZ}, 1.f})};

	for (int j{}; j < height; j++)
		for (int i{}; i < width; i++) {
			const float x{(2.f * (i + .5f) / static_cast<float>(width) - 1.f) * aspectRatio * scale},
			            y{(1.f - 2.f * (j + .5f) / static_cast<float>(height)) * scale};
			const glm::vec3 d{glm::normalize(camera2World * glm::vec4{x, y, -1.f, 0.f})};
			framebuffer.at<cv::Vec3f>(j, i) = glm2Cv(trace({o, d}, objects));
		}

    framebuffer.convertTo(framebuffer,CV_8UC3, 255);

	return framebuffer;
}

glm::vec3 RayTracer::trace(const Ray i, const std::list<Object*>& objects, const unsigned depth)
{
	glm::vec3 color;

	float tMin{1000.f};
	for (const Object* const o : objects) {
		const Object::Intersection hit{o->intersect(i)};
		if (hit.t > 0.f && hit.t < tMin) {
			if (hit.s) {
				if (depth + 1 < maxDepth && hit.s->eta >= 1.f)
                    color = trace(i.castRefractive(hit.p, hit.n, hit.s->eta), objects, depth + 1);
				else if (hit.s->bitmap)
					color = hit.s->bitmap->sample(hit.uv);
			}
			else
                color = {1.f / hit.t, 0.f, 1.f}; // pro objekty bez materialu (debug. ucely), potom lze smazat
			tMin = hit.t;
		}
	}

	return color;
}
