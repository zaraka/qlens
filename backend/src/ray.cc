#include <algorithm>

#include "backend/include/ray.h"

Ray Ray::castReflective(const glm::vec3& p, const glm::vec3& n) const
{
	return {p + eps * n, glm::normalize(glm::reflect(d, n))};
}

Ray Ray::castRefractive(const glm::vec3& p, const glm::vec3& n, float etat) const
{
	float cosi{glm::clamp(glm::dot(d, n), -1.f, 1.f)},
	      etai{1.f};
	glm::vec3 normal{n};
	const bool inside{cosi < 0.f};
	if (inside)
		cosi = -cosi;
	else {
		std::swap(etai, etat);
		normal = -normal;
	}
	const float eta{etai / etat},
	            k{1.f - eta * eta * (1.f - cosi * cosi)};
	return k < 0.f ? castReflective(p, normal)
	               : Ray{inside ? p - eps * n : p + eps * n, glm::normalize(eta * d + (eta * cosi - glm::sqrt(k)) * normal)};
}

const float Ray::eps{.0001f};
