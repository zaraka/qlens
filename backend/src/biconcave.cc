#include "backend/include/biconcave.h"
#include "backend/include/ray.h"

Biconcave::Biconcave(const glm::vec3 pos, const float r1, const float r2, const Material* const s)
{
	const glm::vec3 posA{pos.x, pos.y, pos.z + r1}, posB{pos.x, pos.y, pos.z - r2};
	a = Sphere{posA, r1, s};
	b = Sphere{posB, r2, s};
	c = Sphere{{pos}, glm::length(posA - posB) / 2.f, s};
}

/*
Object::Intersection Biconcave::intersect(const Ray& i) const
{
	Object::Intersection hitA0{}, hitA1{}, hitB0{}, hitB1{}, hitC0{}, hitC1{}, hit0{}, hit1{};
	a.intersect(i, hitA0, hitA1);
	b.intersect(i, hitB0, hitB1);
	c.intersect(i, hitC0, hitC1);
	hit1 = hitC1;

	if (hitC0.t < hitA0.t || !hitA0.t)
		hit0 = hitC0;
	else if (hitA1.t < hitC1.t) {
		hit0 = hitA1;
		hit0.n = -hit0.n;
	}
	else
		return {};

	if (hit0.t < hitB0.t || !hitB0.t)
		return hit0;
	else if (hitB1.t < hit1.t) {
		hitB1.n = -hitB1.n;
		return hitB1;
	}

	return {};
}
*/

Object::Intersection Biconcave::intersect(const Ray& i) const
{
	const std::list<Object::Interval> spansA{a.intersectAll(i)},
	                                  spansB{b.intersectAll(i)},
	                                  spansC{c.intersectAll(i)};

	if (spansC.empty())
		return {};

	Object::Interval span{spansC.front()};
	
	if (!spansA.empty())
		span = {span - spansA.front()};
	if (span.l.empty())
		return {};

	if (!spansB.empty())
		span = {span - spansB.front()};
	if (span.l.empty())
		return {};

	const float t{span.l[Object::Interval::X].t};
	const Object* const o{span.l[Object::Interval::X].o};
	const glm::vec3 p{i.o + t * i.d},
	                n{glm::normalize(p - dynamic_cast<const Sphere*>(o)->c)};
	return {t, p, span.l[Object::Interval::X].invertNormal ? -n : n, {}, o->getSurface()};
}
