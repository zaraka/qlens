#include <glm/detail/func_common.hpp>

#include "backend/include/triangle.h"
#include "backend/include/ray.h"

// Moller-Trumbore algorithm
Object::Intersection Triangle::intersect(const Ray& i) const
{
	const glm::vec3 ba{b.pos - a.pos},
	                ca{c.pos - a.pos},
	                pvec{glm::cross(i.d, ca)};
	const float det{glm::dot(ba, pvec)};
	if (glm::abs(det) < Ray::getEps())
		return {};

	const float invDet{1.f / det};
	const glm::vec3 tvec{i.o - a.pos};
	glm::vec2 uv; // barycentric coordinates
	uv.x = glm::dot(tvec, pvec) * invDet;
	if (uv.x < 0.f || uv.x > 1.f)
		return {};

	const glm::vec3 qvec{glm::cross(tvec, ba)};
	uv.y = glm::dot(i.d, qvec) * invDet;
	if (uv.y < 0.f || uv.x + uv.y > 1.f)
		return {};

	const float t{glm::dot(ca, qvec) * invDet};
	return {t, i.o + t * i.d, glm::normalize(glm::cross(b.pos - a.pos, c.pos - a.pos)), {(1.f - uv.x - uv.y) * a.uv + uv.x * b.uv + uv.y * c.uv}, s};
}
