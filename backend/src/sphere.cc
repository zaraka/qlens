#include <algorithm> // std::swap

#include <glm/geometric.hpp>

#include "backend/include/sphere.h"
#include "backend/include/ray.h"

bool Sphere::solveQuadratic(const float a, const float b, const float c, float& x0, float& x1)
{
	const float discr{b * b - 4.f * a * c};
	if (discr < 0.f)
		return false;
	else if (!discr)
		x0 = x1 = -.5f * b / a;
	else {
		const float q{b > 0.f ? -.5f * (b + glm::sqrt(discr)) : -.5f * (b - glm::sqrt(discr))};
		x0 = q / a;
		x1 = c / q;
	}
	return true;
}

Object::Intersection Sphere::intersect(const Ray& i) const
{
	// analytical solution
	float t0, t1;
	const glm::vec3 oc{i.o - c};
	if (!solveQuadratic(glm::dot(i.d, i.d), 2.f * glm::dot(i.d, oc), glm::dot(oc, oc) - r * r, t0, t1))
		return {};
	if (t0 > t1)
		std::swap(t0, t1);
	if (t0 < 0.f) {
		t0 = t1; // let's try t1 instead
		if (t0 < 0.f)
			return {}; // both t0 and t1 are negative
	}

	const glm::vec3 p{i.o + t0 * i.d};
	return {t0, p, glm::normalize(p - c), {}, s};
}
/*
void Sphere::intersect(const Ray& i, Object::Intersection& hit0, Object::Intersection& hit1) const
{
	float t0, t1;
	const glm::vec3 oc{i.o - c};
	if (!solveQuadratic(glm::dot(i.d, i.d), 2.f * glm::dot(i.d, oc), glm::dot(oc, oc) - r * r, t0, t1))
		return;
	if (t0 > t1)
		std::swap(t0, t1);

	glm::vec3 p{i.o + t0 * i.d};
	hit0 = {t0, p, glm::normalize(p - c), {}, s};
	p = i.o + t1 * i.d;
	hit1 = {t1, p, glm::normalize(p - c), {}, s};
}
*/
std::list<Object::Interval> Sphere::intersectAll(const Ray& i) const
{
	std::list<Object::Interval> spans;

	float t0, t1;
	const glm::vec3 oc{i.o - c};
	if (solveQuadratic(glm::dot(i.d, i.d), 2.f * glm::dot(i.d, oc), glm::dot(oc, oc) - r * r, t0, t1)) {
		if (t0 > t1)
			std::swap(t0, t1);
		spans.push_back({{{t0, this, false}, {t1, this, false}}});
	}

	return spans;
}
