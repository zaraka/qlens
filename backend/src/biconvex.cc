#include "backend/include/biconvex.h"
#include "backend/include/ray.h"
/*
Object::Intersection Biconvex::intersect(const Ray& i) const
{
	Object::Intersection hitA0{}, hitA1{}, hitB0{}, hitB1{};
	a.intersect(i, hitA0, hitA1);
	b.intersect(i, hitB0, hitB1);

	if (hitA0.t < hitB0.t && hitA1.t > hitB0.t)
		return hitB0;
	if (hitB0.t < hitA0.t && hitB1.t > hitA0.t)
		return hitA0;
	return {};
}*/

Object::Intersection Biconvex::intersect(const Ray& i) const
{
	const std::list<Object::Interval> spansA{a.intersectAll(i)},
	                                  spansB{b.intersectAll(i)};
	if (spansA.empty() || spansB.empty())
		return {};

	const Object::Interval span{spansA.front() & spansB.front()};
	if (span.l.empty())
		return {};

	const float t{span.l[Object::Interval::X].t};
	const Object* const o{span.l[Object::Interval::X].o};
	const glm::vec3 p{i.o + t * i.d};
	return {t, p, glm::normalize(p - dynamic_cast<const Sphere*>(o)->c), {}, o->getSurface()};
}
