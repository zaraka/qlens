#include <stdexcept>

#include <opencv2/imgproc/imgproc.hpp>

#include "backend/include/texture.h"

const glm::vec3 Texture::sample(const glm::vec2& uv)
{
	if (m.empty())
		throw std::runtime_error{"Texture::sample: no image data"};

	const cv::Point2f pt{uv.x * (m.cols - 1), m.rows - 1 - uv.y * (m.rows - 1)};
	cv::Mat patch;
	cv::getRectSubPix(m, cv::Size(1, 1), pt, patch);
	const cv::Vec3b texel{patch.at<cv::Vec3b>(0, 0)};

	return {texel[2] / 255.f, texel[1] / 255.f, texel[0] / 255.f}; // reorder & normalize the BGR vector values
}

